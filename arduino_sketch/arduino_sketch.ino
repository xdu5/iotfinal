#include <LiquidCrystal.h>
#include "DHT.h"

//start config
#define DHTPIN 2
#define DHTTYPE DHT22
DHT dht(DHTPIN, DHTTYPE);

const int MOISTURE_PIN = A0;
constexpr int LIGHT_PIN = A1;
const int WATER_PUMP_PIN = 5;

int raw_moisture = 0;
int raw_light = 0;
int moisture = 0;
int light = 0;
LiquidCrystal lcd(7, 8, 9, 10, 11, 12);
//end config

//when start watering and for how long, 
const int startWateringMoisture = 30;
const int stopWateringMoisture = 50;

const long waterTime = 1000L * 15L; //15s
const long sensorReadIntervals = 1000L * 10L; //15s
const long waterIntervals = 1000L * 60L * 60L * 6; //6h
long prevWaterTime = 0;
boolean toWater = false;

unsigned long myTime;

void setup() {
  Serial.begin(9600);

  pinMode(WATER_PUMP_PIN, OUTPUT);
  dht.begin();
  lcd.begin(16, 2);
  lcd.print("Temperature: 76F");
  lcd.setCursor(0,2);
  lcd.print("Humidity: 43%");
  stopWatering();
}

void loop() {
  int temperature = int(getTemperature());
  int humidity = int(getHumidity());
  int raw_moisture = analogRead(MOISTURE_PIN);

  Serial.print("Timestamp: ");
  myTime = millis();
  Serial.println(myTime); // prints time since program started
  delay(1000);          // wait a second so as not to send massive amounts of data

  //moisture(0% = dry, 100% = wet)
  moisture = map(raw_moisture, 630, 200, 0, 100);
  Serial.println("Soil Moisture Level: " + String(moisture) + "%");

  raw_light = analogRead(LIGHT_PIN);
  //light(0% = dark, 100% = bright)
  light = map(raw_light, 0, 1023, 0, 100);
 
  Serial.println("Light Level: " + String(light) + "%");

  Serial.println("Temperature: " + String(temperature) + "F");
  Serial.println("Humidity: " + String(humidity) + "%");
 
  // lcd.print("Temperature: " + String(temperature) + "F");
  // lcd.setCursor(0,2);
  // lcd.print("Humidity: " + String(humidity) + "%");
  
 
  if (millis() - prevWaterTime > waterIntervals) {
    startWatering(moisture);
    prevWaterTime = millis();
  }

  // if (light <= 10) {
  //   Serial.println("Notice: moving the plant!");
  // }
  Serial.println("******************************");
  delay(sensorReadIntervals);
}

void startWatering(int moisture) {
  if (moisture <= startWateringMoisture) {
    toWater = true;
  } else if (moisture > stopWateringMoisture) {
    toWater = false;
  }
  Serial.println(toWater ? "Starting to water" : "Skipping water");
    
  if (toWater) {
    
    Serial.println("Water pump on");
    digitalWrite(WATER_PUMP_PIN, LOW);
    delay(waterIntervals);
    stopWatering();
  }
}

void stopWatering() {
  Serial.println("Water pump off");
  digitalWrite(WATER_PUMP_PIN, HIGH);
}

float getTemperature() {
  float temperature = dht.readTemperature(true);
  if (isnan(temperature)) {
    Serial.println("DHT reading failed!");
  }
  return temperature;
}

float getHumidity() {
  float humidity = dht.readHumidity();
  if (isnan(humidity)) {
    Serial.println("DHT reading failed!");
  }
  return humidity;
}



