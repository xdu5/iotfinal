const { Sequelize, DataTypes, Model } = require("sequelize");
const sequelize = new Sequelize(process.env.DATABASE_URL, {
  dialect: "postgres",
  protocol: "postgres",
  dialectOptions: { ssl: process.env.USE_PG_SSL == "1" },
});

class User extends Model {}
User.init(
  {
    // Model attributes are defined here
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
      defaultValue: "",
    },
  },
  {
    sequelize, // We need to pass the connection instance
    modelName: "User", // We need to choose the model name
  }
);

class Device extends Model {}
Device.init(
  {
    deviceName: {
      type: DataTypes.STRING,
      defaultValue: "",
    },
  },
  { sequelize, modelName: "Device" }
);

class SensorData extends Model {}
SensorData.init(
  {
    soilmoisture: {
      type: DataTypes.DECIMAL(10, 2),
      get: get("soilmoisture"),
    },
    light: {
      type: DataTypes.DECIMAL(10, 2),
      get: get("soilmoisture"),
    },
    temperature: {
      type: DataTypes.INTEGER,
      get: get("temperature"),
    },
    humidity: {
      type: DataTypes.INTEGER,
      get: get("humidity"),
    },
  },
  { sequelize, modelName: "SensorData" }
);

User.hasMany(Device);
Device.belongsTo(User);
SensorData.belongsTo(Device);

module.exports = {
  sequelize,
  SensorData,
  User,
  Device,
};

// the defined model is the class itself
// console.log(User === sequelize.models.User); // true
