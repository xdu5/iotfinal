// const express = require("express");
// const path = require("path");

// const PORT = process.env.PORT || 5001;

// express()
//   .use(express.static(path.join(__dirname, "public")))
//   .set("views", path.join(__dirname, "views"))
//   .set("view engine", "ejs")
//   .get("/", (req, res) => res.render("pages/index"))
//   .listen(PORT, () => console.log(`Listening on ${PORT}`));

const { Op } = require("sequelize");
const bodyParser = require("body-parser");
const { SensorData } = require("./database");

const app = express();
const express = require("express");
const router = express.Router();

// Routes
router.post("/sensordata", (req, res, next) => {
  return SensorData.create(obsBody).then((data) => {
    return res.json(data.toJSON());
  });
});
router.get("/sensordata", (req, res, next) => {
  const limit = req.query.limit || 24;
  const wh = {};
  return SensorData.findAll({
    limit,
    wh,
    order: [["id", "DESC"]],
  }).then((sensordata) => {
    return res.json(sensordata.map((data) => data.toJSON()));
  });
});

app.use(bodyParser.urlencoded());
app.use(bodyParser.json());
app.use(bodyParser.text());
app.use(router);

module.exports = app;
